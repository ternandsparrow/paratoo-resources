FROM node:16-alpine
LABEL maintainer="monitor"
VOLUME /public
WORKDIR /srv/http-server
COPY package.json yarn.lock ./
RUN yarn install --production
EXPOSE 9191
ENTRYPOINT ["yarn", "run", "http-server", "-p", "9191" ,"--cors"]